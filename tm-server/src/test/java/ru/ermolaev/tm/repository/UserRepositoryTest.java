package ru.ermolaev.tm.repository;

import org.junit.*;
import ru.ermolaev.tm.api.repository.IUserRepository;
import ru.ermolaev.tm.dto.UserDTO;

import java.util.Arrays;
import java.util.List;

public class UserRepositoryTest {

    /*
        Требуется рефакторинг в связи с переходом на новую моель хранения данных
    */

//    final private IUserRepository userRepository = new UserRepository();
//
//    final private static UserDTO USER_DTO_ONE = new UserDTO();
//
//    final private static UserDTO USER_DTO_TWO = new UserDTO();
//
//    final private static UserDTO USER_DTO_THREE = new UserDTO();
//
//    final private static UserDTO[] USER_DTO_ARRAY = {USER_DTO_ONE, USER_DTO_TWO, USER_DTO_THREE};
//
//    final private static List<UserDTO> USER_DTO_LIST = Arrays.asList(USER_DTO_ARRAY);
//
//    @BeforeClass
//    public static void initData() {
//        USER_DTO_ONE.setLogin("test");
//        USER_DTO_ONE.setEmail("test@test.ru");
//
//        USER_DTO_TWO.setLogin("admin");
//        USER_DTO_TWO.setEmail("admin@admin.ru");
//
//        USER_DTO_THREE.setLogin("user");
//        USER_DTO_THREE.setEmail("user@user.ru");
//    }
//
//    @Before
//    public void addUser() {
//        userRepository.add(USER_DTO_ONE);
//    }
//
//    @After
//    public void deleteUser() {
//        userRepository.clear();
//    }
//
//    @Test
//    public void createEmptyUserRepositoryTest() {
//        final IUserRepository userRepository = new UserRepository();
//        Assert.assertTrue(userRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void createNotEmptyUserRepositoryTest() {
//        final IUserRepository userRepository = new UserRepository();
//        userRepository.add(new UserDTO());
//        Assert.assertFalse(userRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void findByIdTest() {
//        final UserDTO userDTO = userRepository.findById(USER_DTO_ONE.getId());
//        Assert.assertNotNull(userDTO);
//        Assert.assertEquals(userDTO.getId(), USER_DTO_ONE.getId());
//    }
//
//    @Test
//    public void findByLoginTest() {
//        final UserDTO userDTO = userRepository.findByLogin(USER_DTO_ONE.getLogin());
//        Assert.assertNotNull(userDTO);
//        Assert.assertEquals(userDTO.getLogin(), USER_DTO_ONE.getLogin());
//    }
//
//    @Test
//    public void findByEmailTest() {
//        final UserDTO userDTO = userRepository.findByEmail(USER_DTO_ONE.getEmail());
//        Assert.assertNotNull(userDTO);
//        Assert.assertEquals(userDTO.getEmail(), USER_DTO_ONE.getEmail());
//    }
//
//    @Test
//    public void findAllTest() {
//        userRepository.add(USER_DTO_TWO);
//        final List<UserDTO> userDTOS = userRepository.findAll();
//        Assert.assertNotNull(userDTOS);
//        Assert.assertEquals(2, userDTOS.size());
//    }
//
//    @Test
//    public void removeUserTest() {
//        userRepository.remove(USER_DTO_ONE);
//        Assert.assertTrue(userRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void removeByIdTest() {
//        userRepository.removeById(USER_DTO_ONE.getId());
//        Assert.assertTrue(userRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void removeByLoginTest() {
//        userRepository.removeByLogin(USER_DTO_ONE.getLogin());
//        Assert.assertTrue(userRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void removeByEmailTest() {
//        userRepository.removeByEmail(USER_DTO_ONE.getEmail());
//        Assert.assertTrue(userRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void addTest() {
//        final UserDTO userDTO =  userRepository.findById(USER_DTO_ONE.getId());
//        Assert.assertNotNull(userDTO);
//        Assert.assertEquals(userDTO.getId(), USER_DTO_ONE.getId());
//    }
//
//    @Test
//    public void addCollectionTest() {
//        userRepository.add(USER_DTO_LIST);
//        final List<UserDTO> userDTOS = userRepository.findAll();
//        Assert.assertNotNull(userDTOS);
//        Assert.assertEquals(4, userDTOS.size());
//    }
//
//    @Test
//    public void addVarargsTest() {
//        userRepository.add(USER_DTO_ARRAY);
//        final List<UserDTO> userDTOS = userRepository.findAll();
//        Assert.assertNotNull(userDTOS);
//        Assert.assertEquals(4, userDTOS.size());
//    }
//
//    @Test
//    public void clearTest() {
//        userRepository.add(USER_DTO_LIST);
//        final List<UserDTO> userDTOS = userRepository.findAll();
//        Assert.assertNotNull(userDTOS);
//        Assert.assertEquals(4, userDTOS.size());
//        userRepository.clear();
//        Assert.assertTrue(userRepository.findAll().isEmpty());
//    }
//
//    @Test
//    public void loadCollectionTest() {
//        userRepository.load(USER_DTO_LIST);
//        final List<UserDTO> userDTOS = userRepository.findAll();
//        Assert.assertNotNull(userDTOS);
//        Assert.assertEquals(3, userDTOS.size());
//    }
//
//    @Test
//    public void loadVarargsTest() {
//        userRepository.load(USER_DTO_ARRAY);
//        final List<UserDTO> userDTOS = userRepository.findAll();
//        Assert.assertNotNull(userDTOS);
//        Assert.assertEquals(3, userDTOS.size());
//    }

}
